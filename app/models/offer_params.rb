class OfferParams < Struct.new(:uid, :pub0, :page)
	include ActiveModel::Model
	include ActiveModel::Validations

	validates :uid, :page, presence: true

	def self.from_hash(hash)
		new.tap do |obj|
			hash.each {|key,value| obj.send("#{key}=", value)}
		end
	end
end