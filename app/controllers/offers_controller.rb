class OffersController < ApplicationController
	def new
		@offer_params = OfferParams.from_hash(page: 1)
	end

	def create
		@offer_params = OfferParams.from_hash(offer_params)
		if @offer_params.valid?
			service = FyberApi::OffersService.new(@offer_params)
			begin
				data = service.fetch_data.with_indifferent_access
				@offers = data['offers']
				render :show
			rescue FyberApi::OffersService::BadRequest
				flash[:error] = "Invalid request. Try again"
				render :new
			end
		else
			render :new
		end
	end

	private
	def offer_params
		params.require(:offer_params).permit(:uid, :page, :pub0)
	end
end
