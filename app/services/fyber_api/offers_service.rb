require 'digest/sha1'

module FyberApi
	class OffersService
		BadRequest = Class.new(Exception)
		BadIntegrity = Class.new(Exception)

		def initialize(params)
			@params = params
		end

		def fetch_data
			response = perform_request(construct_url)
			if response.is_a?(Net::HTTPSuccess)
				body = response.body
				check_integrity(response)
				JSON.parse(response.body)
			else
				raise BadRequest
			end
		end

		private

		# returns URI object
		def construct_url
			config = FyberConfig.get
			uri = URI('http://api.sponsorpay.com/feed/v1/offers.json')
			params = {
				appid: 		config.appid,
				uid: 		@params.uid,
				ip: 		config.ip,
				locale: 	config.locale,
				device_id: 	config.device_id,
				timestamp: 	Time.now.to_i,
				offer_types: config.offer_types,
				os_version: '4.1.1', # ???
				format: 	'json'
			}
			params[:pub0] = @params.pub0 if @params.pub0
			params[:hashkey] = generate_hashkey(params)
			uri.query = URI.encode_www_form(params)
			uri
		end

		def perform_request(uri)
			Net::HTTP.get_response(uri)
		end

		# generates a hash from given parameters as described at http://developer.fyber.com/content/android/offer-wall/offer-api/
		def generate_hashkey(params)
			sorted_params = Hash[params.sort]
			params_string = URI.encode_www_form(sorted_params) + '&' + FyberConfig.get.api_key
			digest = Digest::SHA1.hexdigest params_string
			digest
		end

		def check_integrity(response)
			header = response['X-Sponsorpay-Response-Signature']
			checksum = Digest::SHA1.hexdigest(response.body + FyberConfig.get.api_key)
			if header != checksum
				raise BadIntegrity
			end
		end
	end
end