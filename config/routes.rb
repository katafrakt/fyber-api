Rails.application.routes.draw do
  resource :offer, only: [:new, :create]
  root to: 'offers#new'
end
