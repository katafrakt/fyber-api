class FyberConfig < OpenStruct
	def load_config
		config = YAML.load_file(Rails.root.join('config', 'fyber.yml').to_s)
		config.fetch(Rails.env, {}).each do |key, val|
			send("#{key}=", val)
		end
	end

	def self.get
		@@config ||= new.tap {|obj| obj.load_config }
	end
end