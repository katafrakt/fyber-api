require_relative '../rails_helper'

describe OffersController do
	let(:request_params) { {offer_params: {uid: 'player1', page: 1}} }

	before(:each) do
		allow_any_instance_of(FyberApi::OffersService).to receive(:construct_url) { ' ' } # stub out url creation
	end

	context 'new' do
	end

	context 'create' do
		render_views

		context 'with empty offers' do
			let(:fake_response) { {'offers' => []} }
			before(:each) do
				allow_any_instance_of(FyberApi::OffersService).to receive(:fetch_data) { fake_response }
			end

			it 'should display No offers' do
				post :create, request_params
				expect(response.body).to match(/No offers/)
			end
		end

		context 'with sample offer' do
			let(:fake_response) { JSON.parse(IO.read(Rails.root.join("spec", "fixtures", "fake_response.json"))) }

			before(:each) do
				allow_any_instance_of(FyberApi::OffersService).to receive(:fetch_data) { fake_response }
			end

			it 'should contain offer name' do
				post :create, request_params
				expect(response.body).to match(/Tap  Fish/)
			end

			it 'should contain lowres thumbnail' do
				post :create, request_params
				expect(response.body).to match(/icon175x175-2_square_60.png/)
			end

			it 'should not contain hires thumbnail' do
				post :create, request_params
				expect(response.body).not_to match(/icon175x175-2_square_175.png/)
			end
		end

		context 'with bad request' do
			before(:each) do
				allow_any_instance_of(FyberApi::OffersService).to receive(:fetch_data) { raise FyberApi::OffersService::BadRequest }
			end

			it 'should render new action' do
				post :create, request_params
				expect(response).to render_template('new')
			end

			it 'should contain message in flash' do
				post :create, request_params
				expect(flash[:error]).to match(/Invalid request/)
			end
		end
	end
end