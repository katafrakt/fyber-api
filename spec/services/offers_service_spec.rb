require_relative '../rails_helper'

class Fake200Response < Net::HTTPSuccess
  def body
    {hello: 'world'}.to_json
  end
end

describe FyberApi::OffersService do
	let(:params) { OfferParams.from_hash(page: 1, uid: 'test user') }
	let(:service) { FyberApi::OffersService.new(params) }


	context 'response' do
		before(:each) do
			allow(service).to receive(:construct_url) { ' ' } # stub out url creation
		end

		it 'should raise error on 401 response' do
			allow(service).to receive(:perform_request) { Net::HTTPUnauthorized.new(1.0, 401, "401 Unauthorized") }
			expect{service.fetch_data}.to raise_error(FyberApi::OffersService::BadRequest)
		end

		it 'should return JSON on 200 response' do
			allow(service).to receive(:perform_request) { Fake200Response.new(1.0, 200, '200 Success') }
			expect(service.fetch_data).to be_a(Hash)
		end
	end

	# testing private method, but it contains important business logic
	context 'hash generator' do
		let(:params) do
			{
				zzz: 'aaBB',
				question: 'is it really?',
			}
		end

		before(:each) do
			allow(FyberConfig).to receive(:get) { OpenStruct.new.tap {|obj| obj.api_key = '12345678'} }
		end

		it 'should return correct hashkey' do
			expect(service.send(:generate_hashkey, params)).to eq('28284c7eed7325111d46cecc630802dc84ea8621')
		end
	end
end